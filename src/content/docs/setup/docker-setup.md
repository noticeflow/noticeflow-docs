---
title: Deploy with Docker
sidebar:
  order: 1
---
The recommended method of installing NoticeFlow is to use our latest Docker image. This image is automatically built and published to our GitLab Container Registry whenever a new release is made. 

The Docker image is available at `registry.gitlab.com/noticeflow/noticeflow-tool:latest`.

## Prerequisites

* A MySQL database server, either running locally or on a remote server. The database server must be accessible from the machine running NoticeFlow.
* Install [Docker](https://docs.docker.com/get-docker/)
* Install [Docker Compose](https://docs.docker.com/compose/install/). Although not strictly necessary, Docker Compose makes it much easier to deploy NoticeFlow.

## Deployment

The simplest way to deploy NoticeFlow is to use Docker Compose. To do so, create a `docker-compose.yml` file with the following contents:

```yaml title="docker-compose.yml"
version: "3"
services:
  noticeflow-tool:
    image: registry.gitlab.com/noticeflow/noticeflow-tool:latest
    depends_on:
      - db
    ports:
    - 3000:3000
    environment:
      DATABASE_URL: ${DATABASE_URL}
      NEXTAUTH_SECRET: ${NEXTAUTH_SECRET}
      NEXTAUTH_URL: ${NEXTAUTH_URL}

      # SAML configuration
      SAML_SSO_LOGIN_URL: ${SAML_SSO_LOGIN_URL}
      SAML_SSO_LOGOUT_URL: ${SAML_SSO_LOGOUT_URL}
      SAML_ENTITY_ID: ${SAML_ENTITY_ID}
      SAML_IDP_PUBLIC_KEY: ${SAML_IDP_PUBLIC_KEY}
      SAML_SP_PUBLIC_KEY: ${SAML_SP_PUBLIC_KEY}
      SAML_SP_PRIVATE_KEY: ${SAML_SP_PRIVATE_KEY}

      ADMIN_EMAIL: ${ADMIN_EMAIL}

      SETTINGS_ENCRYPTION_KEY: ${SETTINGS_ENCRYPTION_KEY}

  # For production, you should most likely use a separate database server.
  db:
    image: mysql:8.0
    environment:
      MYSQL_ROOT_PASSWORD: "secret"
      MYSQL_DATABASE: "noticeflow"
    volumes:
      - db_data:/var/lib/mysql

volumes:
  db_data:
```

Then, create a `.env` file with the following contents:

```bash frame="none"
ADMIN_EMAIL="admin@example.com"

DATABASE_URL="mysql://root:secret@db:3306/noticeflow"
NEXTAUTH_SECRET="secret goes here"
NEXTAUTH_URL="https://noticeflow.example.com"

SAML_SSO_LOGIN_URL="https://mocksaml.com/api/saml/sso"
SAML_SSO_LOGOUT_URL="https://mocksaml.com/api/saml/slo"
SAML_ENTITY_ID="https://saml.example.com/entityid"
SAML_IDP_PUBLIC_KEY=""
SAML_SP_PUBLIC_KEY=""
SAML_SP_PRIVATE_KEY=""

SETTINGS_ENCRYPTION_KEY="secret goes here"
```
