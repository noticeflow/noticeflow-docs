---
title: What is NoticeFlow?
sidebar:
  order: 1
---

NoticeFlow is an open-source and free application that allows you to track and manage incoming DMCA complaints, helping you stay compliant with the DMCA and other similar laws. It is geared towards universities and other entities that receive a large number of DMCA complaints and need ways to improve the efficiency of their DMCA compliance process.

## How does it work?

The core of NoticeFlow is the ability to automatically ingest and process DMCA notices. Once ingested, NoticeFlow will automatically parse the notice and extract relevant information, such as the infringing content, the complainant, and the alleged infringer. The notice will then be tracked by the system, allowing users to begin the process of resolving the notice.

## What features does it have?

NoticeFlow has a number of features that make it a powerful tool for DMCA compliance. These include:
* **Automatic Notice Ingestion**: NoticeFlow can automatically ingest notices from a variety of sources, including email inboxes and its web API.
* **Notice Tracking**: NoticeFlow tracks notices throughout the entire DMCA compliance process, from initial ingestion to resolution.
* **Templated Email Responses**: NoticeFlow can automatically generate templated email responses to notices, allowing faster response times.
* **Automated Investigation**: NoticeFlow can automatically begin investigating the infringement described in the notice upon receipt, allowing faster resolution.
* **Automated Remediation**: NoticeFlow allows you to integrate your own webhooks that can be called to automatically remediate the infringement, such as removing network access from the infringer.
* **Analytics and Auditing**: NoticeFlow provides analytics and auditing tools to help you track your DMCA compliance process and identify areas for improvement.