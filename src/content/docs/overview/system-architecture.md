---
title: System Architecture
sidebar:
  order: 2
---

NoticeFlow has two main components:

* **NoticeFlow Application**: The NoticeFlow application is a web server that provides a web interface for managing DMCA notices. It also provides a REST API that can be used to integrate with other systems.
* **MySQL database**: The MySQL database stores all of the data used by the NoticeFlow application.

The most effective way to deploy NoticeFlow is to use **Docker Compose**. Docker Compose allows you to easily deploy both the NoticeFlow application and the MySQL database. For more information on deploying NoticeFlow, see the [Deployment](/docs/guides/deployment) guide.