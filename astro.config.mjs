import { defineConfig } from "astro/config";
import starlight from "@astrojs/starlight";

// https://astro.build/config
export default defineConfig({
  integrations: [
    starlight({
      title: "NoticeFlow",
      logo: {
        light: "./src/assets/svg/NF-LOGO-RGB_HORIZONTAL.svg",
        dark: "./src/assets/svg/NF-LOGO-RGB_HORIZONTAL-INV.svg",
        replacesTitle: true,
      },
      customCss: ["./src/styles/custom.css"],
      social: {
        gitlab: "https://gitlab.com/noticeflow/noticeflow-tool",
      },
      sidebar: [
        {
          label: "Overview",
          autogenerate: { directory: "overview" },
        },
        {
          label: "Setup & Deployment",
          autogenerate: { directory: "setup" },
        },
        {
          label: "Reference",
          autogenerate: { directory: "reference" },
        },
      ],
    }),
  ],
});
